#Author: your.email@your.domain.com

@tokenPayment
Feature: Payment with token

  @tokenSuccess
  Scenario: Title of your scenario
    Given customer has bank account
    And customer has account in DTUpay
    And customer has at least one unused token
    And merchant has bank account
    And merchant has account in DTUpay
    When merchant scans the token 
    Then the payment succeeds
    And money transfered from the customer account to merchant account in the bank

	@reuseTokenFailure
  Scenario: failure payment with used token
    Given customer has bank account
    And customer has account in DTUpay
    And merchant has bank account
    And merchant has account in DTUpay
    And customer has one used token
    When merchant scans the token 
    Then the payment fails
    And customer and merchant have the same amount of money as before
    
  @fakeTokenFailure
	Scenario: failure payment with fake token
    Given customer has bank account
    And customer has account in DTUpay
    And customer has one fake token
    And merchant has bank account
    And merchant has account in DTUpay
    When merchant scans the token 
    Then the payment fails
    And customer and merchant have the same amount of money as before