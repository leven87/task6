package helloservice;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class TokenPaymentSteps {
    
    private WebTarget apiPaymentUrl;
    
    public TokenPaymentSteps() {
        Client client = ClientBuilder.newClient();
        apiPaymentUrl = client.target("http://localhost:8080/");
    }

    @Given("customer has bank account")
    public void customerHasBankAccount() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Given("customer has account in DTUpay")
    public void customerHasAccountInDTUpay() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Given("customer has at least one unused token")
    public void customerHasAtLeastOneUnusedToken() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Given("merchant has bank account")
    public void merchantHasBankAccount() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Given("merchant has account in DTUpay")
    public void merchantHasAccountInDTUpay() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @When("merchant scans the token")
    public void merchantScansTheToken() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Then("the payment succeeds")
    public void thePaymentSucceeds() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Then("money transfered from the customer account to merchant account in the bank")
    public void moneyTransferedFromTheCustomerAccountToMerchantAccountInTheBank() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Given("customer has one used token")
    public void customerHasOneUsedToken() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Then("the payment fails")
    public void thePaymentFails() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Then("customer and merchant have the same amount of money as before")
    public void customerAndMerchantHaveTheSameAmountOfMoneyAsBefore() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Given("customer has one fake token")
    public void customerHasOneFakeToken() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }


}
