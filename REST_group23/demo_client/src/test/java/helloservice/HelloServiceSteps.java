package helloservice;

import static org.junit.Assert.assertEquals;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

/* Hint:
 * The step classes directly access the code to create URL requests.
 * Ideally, this code should be hidden behind an interface using the
 * dependency inversion principle also in test code, to make writing
 * step definitions easier.
 * 
 * I didn't do this here to keep the code more focused on what it should
 * demonstrate (i.e. how to call a REST Web service).
 */
public class HelloServiceSteps {

	String result;
	WebTarget baseUrl;

	public HelloServiceSteps() {
		Client client = ClientBuilder.newClient();
		baseUrl = client.target("http://localhost:8080/");
	}
	
	@When("I call the hello service")
	public void iCallTheHelloService() {
		result = baseUrl.path("hello").request().get(String.class);
	}

	@Then("I get the answer {string}")
	public void iGetTheAnswer(String string) {
		assertEquals(string,result);
	}

}
