package com.example.demo.system;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;

public class DTUPay {
    
    BankService bank;
    
    List<Token> tokenList = new ArrayList<>();
    HashMap<String, Customer> customerList = new HashMap<>();
    HashMap<String, Merchant> merchantList = new HashMap<>();
    HashMap<String, List<Token>> cprToTokens = new HashMap<>();
    
    public DTUPay(BankService bankService) {
        this.bank = bankService;
    }
    
    public void registerCustomer(String customerCPR, String firstName, String lastName) {
        Customer customer = new Customer(customerCPR, firstName, lastName);
        customerList.put(customerCPR, customer);
        Token token = new Token();
        tokenList.add(token);
        cprToTokens.put(customerCPR, tokenList);
    }
    
    public void registerMerchant(String merchantCPR, String firstName, String lastName) {
        Merchant merchant = new Merchant(merchantCPR, firstName, lastName);
        merchantList.put(merchantCPR, merchant);
    }

    public List<Token> requestToken() {
        Token token = new Token();
        tokenList.add(token);
        
        return tokenList;
    }
    
    public boolean payWithToken(int amount, String cprCustomer,
            String cprMerchant, Token token) {
        String description = String.format("DTUPay transaction with token %s", token.getTokenId());
        try {
            bank.transferMoneyFromTo(cprCustomer, cprMerchant, new BigDecimal(amount), description);
            return true;
        } catch (BankServiceException_Exception exc) {
            return false;
        }
    }
    
}
