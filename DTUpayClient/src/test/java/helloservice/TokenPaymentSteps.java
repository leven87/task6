package helloservice;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import io.cucumber.java.After;
import dtu.ws.fastmoney.*;
import dtupay.rest.dto.DtuPayUserRepresentation;
import dtupay.rest.dto.Token;
import helloservice.DtuPay;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


public class TokenPaymentSteps {
    
	private BankService bank = new BankServiceService().getBankServicePort();
	private List<String> accounts = new ArrayList<>();
	private DtuPay dtuPay = new DtuPay(bank);
	private List<Token> tokens;
	private Token merchantToken;
	private boolean success;
	private BigDecimal customerBalance;
	private BigDecimal merchantBalance;
	private int amount;
	private String customerCprNumber;
	private String merchantCprNumber;
	private String customerAccount;
	private String merchantAccount;

/*	@After
	public void cleanupUsedAccounts() throws BankServiceException_Exception {
		for (String a : accounts) {
			bank.retireAccount(a);
		}
	}  */
	
  
    @Given("customer has bank account")
    public void customerHasBankAccount() throws BankServiceException_Exception {
		User u = new User();
		customerCprNumber = "280980-0728";
		u.setCprNumber(customerCprNumber);
		u.setFirstName("lee");
		u.setLastName("chuan");
		customerBalance = new BigDecimal(1000);
		customerAccount = bank.createAccountWithBalance(u, customerBalance);
		accounts.add(customerAccount); // Remember created accounts for cleanup
		dtuPay.registerCustomer(u.getCprNumber()
				,u.getFirstName()
				,u.getLastName()
				,customerAccount);
    }

    @Given("customer has account in DTUpay")
    public void customerHasAccountInDTUpay() {
		tokens.add(new Token());
    	//tokens = dtuPay.requestTokens(customerCprNumber,1);
		//assertTrue(tokens.size() >= 1);
    }

    @Given("customer has at least one unused token")
    public void customerHasAtLeastOneUnusedToken() {
    	//merchantToken = tokens.get(0);
    	merchantToken = new Token();
    }

    @Given("merchant has bank account")
    public void merchantHasBankAccount() throws BankServiceException_Exception {
		User u = new User();
		u.setCprNumber("290185-1894");
		u.setFirstName("rui");
		u.setLastName("huang");
		merchantBalance = new BigDecimal(1000);
		merchantAccount = bank.createAccountWithBalance(u, merchantBalance);
		accounts.add(merchantAccount); // remember created accounts for cleanup
		dtuPay.registerMerchant(u.getCprNumber(),u.getFirstName(),u.getLastName(),merchantAccount);
    }

    @When("merchant scans the token")
    public void merchantScansTheToken() {
    	//merchantToken = tokens.get(0);
    	merchantToken = new Token();
    }
    
    @When("requests payment for {int} kroner using the token")
	public void requestsPaymentForKronerUsingTheToken(int amt) throws Throwable {
		BigDecimal amount = new BigDecimal(amt);
	    success = dtuPay.transfer(merchantToken,customerAccount,merchantAccount,amount);
		//success=true;
	}

    @Then("the payment succeeds")
    public void thePaymentSucceeds() {
    	assertTrue(success);
    }

    @Then("money transfered from the customer account to merchant account in the bank")
    public void moneyTransferedFromTheCustomerAccountToMerchantAccountInTheBank() throws BankServiceException_Exception {
    	assertEquals(customerBalance.subtract(new BigDecimal(amount)),bank.getAccount(customerAccount).getBalance());
		assertEquals(merchantBalance.add(new BigDecimal(amount)),bank.getAccount(merchantAccount).getBalance());
    }


}
