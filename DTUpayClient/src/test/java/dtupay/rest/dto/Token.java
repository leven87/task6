package dtupay.rest.dto;

import java.util.UUID;

public class Token {
	
    private String tokenString;
    private boolean used;
	
    public Token() {
        this.tokenString = UUID.randomUUID().toString();
    	this.used = false;
    }
	public Token(String c) {
		tokenString= c;
	}
	
    public String getTokenId() {
        return tokenString;
    }
    
	public void setTokenUsed() {
        this.used = true;
    }
	
    public boolean getTokenUsed() {
        return used;
    }
	
/*	@Override
	public boolean equals(Object other) {
		if (!(other instanceof Token)) {
			return false;
		}
		Token otherToken = (Token) other;
		return value.equals(otherToken.value);
	}
*/

}
