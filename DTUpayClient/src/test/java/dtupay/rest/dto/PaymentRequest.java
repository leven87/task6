package dtupay.rest.dto;

import dtupay.rest.dto.Token;
import java.math.BigDecimal;

public class PaymentRequest {
	private BigDecimal amount;
	private String merchantAcct;
	private Token token;
	private String customerAcct;

	
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getMerchantAcct() {
		return merchantAcct;
	}
	public String getCustomerAcct() {
		return customerAcct;
	}
	
	public void setMerchantAcct(String merchantacct) {
		this.merchantAcct = merchantacct;
	}
	
	public void setCustomerAcct(String customeracct) {
		this.customerAcct = customeracct;
	}
	public Token getToken() {
		return token;
	}
	public void setToken(Token token) {
		this.token = token;
	}

	}

