package dtupay.main;

import java.util.ArrayList;
import java.util.List;

import dtupay.main.Token;

public class TokenManager {
	
	private Customer customer;
	private Merchant merchant;
	ArrayList<Token> tokenArrayList = new ArrayList();
	
	public ArrayList<Token> requestTokens() {
	    Token token = new Token();
	    tokenArrayList.add(token);
		return tokenArrayList;
	}
	
	public void registerCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getCustomerAccountForToken(Token token) {
		return customer.getAccount();
	}

	public void registerMerchant(Merchant merchant) {
		this.merchant = merchant;
	}
	
	public String getMerchantAccountForToken(Token token) {
		return merchant.getAccount();
	}
}
