package dtupay.main;

public class DtuPayUser {
	
	String firstName;
	String lastName;
	String CPR;
	String account;
	
	public DtuPayUser(String cprNumber, String firstName, String lastName, String account) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.account = account;
		this.CPR=cprNumber;
	}
	
	public String getCprNumber() {
		return this.CPR;
	}
	

	public String getAccount() {
		return account;
	}



}
