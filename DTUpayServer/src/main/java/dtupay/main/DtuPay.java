package dtupay.main;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

import dtupay.main.Token;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;


public class DtuPay {
	
	private BankService bank;
	private TokenManager tokenManager = new TokenManager();
	private Merchant merchant;
	HashMap<String,String> acctInfo = new HashMap<String,String>();
	HashMap<String,ArrayList<Token>> acctToken = new HashMap<String,ArrayList<Token>>();

	public DtuPay(BankService bank) {
		this.bank = bank;
	}

	public void registerCustomer(String cpr
			,String firstName
			,String lastName
			,String account) {
		Customer customer = new Customer(cpr
				,firstName
				,lastName
				,account);		
		tokenManager.registerCustomer(customer);
        tokenManager.requestTokens();
		acctToken.put(cpr,tokenManager.tokenArrayList);
	}

	public void registerMerchant(String cpr
			,String firstName
			,String lastName
			,String account) {
		merchant = new Merchant(cpr
				,firstName
				,lastName
				,account);
		tokenManager.registerMerchant(merchant);
        tokenManager.requestTokens();
		acctToken.put(cpr,tokenManager.tokenArrayList);
	}

	public ArrayList<Token> requestTokens(String cprNumber, int number) {
		return tokenManager.requestTokens();
	}

	
	public boolean transfer(Token token,String customerId, String merchantId, BigDecimal amount) throws BankServiceException_Exception {
		if (!token.getTokenUsed() && tokenManager.tokenArrayList.contains(token)) {
    		bank.transferMoneyFromTo(customerId, merchantId, amount, "DTUPay Transaction");
    		tokenManager.tokenArrayList.get(0).setTokenUsed();
    		return true;
		}
		return false;
	}
}
