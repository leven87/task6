package dtupay.rest.dto;

public class TokenRequest {
	
	private String CPR;
	private int number;
	
	public String getCpr() {
		return CPR;
	}
	public void setCpr(String cpr) {
		this.CPR = cpr;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	} 

}