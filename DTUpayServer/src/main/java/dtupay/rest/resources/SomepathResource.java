package dtupay.rest.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import dtupay.main.DtuPay;
import dtupay.main.Token;
import dtupay.rest.dto.DtuPayUserRepresentation;
import dtupay.rest.dto.PaymentRequest;
import dtupay.rest.dto.TokenRequest;

/* Note that the resource names and the exclusive use of HTTP POST is on purpose.
 * You should be designing the right resource URI's and use the correct HTTP verb yourself.
 */
@Path("/somepath")
public class SomepathResource {
	
	
	private static DtuPay dtuPay = new DtuPay(new BankServiceService().getBankServicePort());
	
	@POST
	@Path("more1")
	@Consumes("application/json")
	@Produces("text/plain")
	public String registerCustomer(DtuPayUserRepresentation c) {
		dtuPay.registerCustomer(c.getCpr()
				, c.getFirstName()
				, c.getLastName()
				, c.getAccount());
		return c.getCpr();
	}
	
	@POST
	@Path("more2")
	@Consumes("application/json")
	@Produces("text/plain")
	public String registerMerchant(DtuPayUserRepresentation m) {
		dtuPay.registerMerchant(m.getCpr()
				, m.getFirstName()
				, m.getLastName()
				, m.getAccount());
		return m.getCpr();
	}
	
	@POST
	@Path("more3")
	@Consumes("application/json")
	@Produces("application/json")
	public List<Token> requestTokens(TokenRequest r) {
		return dtuPay.requestTokens(r.getCpr(), r.getNumber());
	}
	
	@POST
	@Path("more4")
	@Consumes("application/json")
	@Produces("text/plain")
	public boolean pay(PaymentRequest p) throws BankServiceException_Exception {
		return dtuPay.transfer(p.getToken()
				, p.getCustomerAcct()
				, p.getMerchantAcct()	
				, p.getAmount());
	}
}
